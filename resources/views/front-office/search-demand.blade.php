@extends('layouts.app')
@section('content')
<div style="height: 70vh">
    <div class="container-fluid !direction !spacing col-10 mx-auto col-md-6 col-lg-6" style="margin-top: 130px">
        <div class="text-center">
            <h3 class="text-bold mx-auto mt-5">Vérifier le statut de votre demande</h3>
        </div>
        <form action="{{ route('search.submit') }}" method="POST">
            @csrf
            <div class="form-floating col-12">
                <input name="demand_code" type="text" class="form-control" id="floatingInput" placeholder="xxxxxxxx" />
                <label for="floatingInput">Code de la demande ou numéro</label>
            </div>
            @error('demand_code')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
            <button id="submit-button" type="submit" class="btn btn-primary mt-3">
                Rechercher
            </button>
        </form>
    </div>
</div>
@endsection