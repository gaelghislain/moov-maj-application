@extends('layouts.app')
@section('content')
    <main class="container-fluid" style="min-height: 100vh">
        <section class="section mb-5">
            <div class="row col-12  mx-auto" style="margin-top: 110px">
                <div class="col-md-6 col-12 mx-auto row justify-content-center">
                    <div class="col-12 col-xl-8 mx-auto" style="border-radius: 15px">
                        <img id="homeImg" src="{{ asset('front-office/img/accueil.png') }}" class="img-fluid"
                            style="border-radius: 15px" alt="Responsive image" />
                    </div>
                </div>
                <div class="col-md-6 mt-2">
                    <div class="text-center mt-4 mx-auto">
                        <h1 style="font-family: 'Philosopher', sans-serif; color:#005aa4e7; font-weight:bold;">
                            Faites désormais votre demande de
                            <span style="color: #f07c01">mise à jour de numéro</span> ici
                        </h1>
                    </div>
                    <div class="d-flex justify-content-center mt-5 col-md-12">
                        <a href="/new-demand" class="btn btn-primary mx-auto p-3 border-4 rounded"
                            style="color:#005aa4e7; font-weight:700; background-color:#fff; border-color:#005aa4e7">Faire
                            une
                            demande de mise à jour</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section mb-5 mt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6  mt-5 text-center">
                        <h1 style="color:#005aa4e7;">Contactez nous</h1>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-12">
                        <div class="wrapper">
                            <div class="row no-gutters">
                                <div class="col-md-5 d-flex align-items-stretch text-dark">
                                    <div class="info-wrap w-100 p-lg-5 p-4">
                                        <h3 class="mb-4 mt-md-4 text-dark">Informations</h3>
                                        <div class="dbox w-100 d-flex align-items-start" style="gap: 20px">
                                            <div class="icon mt-4 d-flex align-items-center justify-content-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" style="color:#005aa4e7"
                                                    fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                                    <path
                                                        d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
                                                </svg>
                                            </div>
                                            <div class="text pl-3">
                                                <p>
                                                    Avenue Jean Paul II Zone résidentielle Immeuble Etisalat-Bénin 01 BP
                                                    8052 Cotonou, Bénin
                                                </p>
                                            </div>
                                        </div>
                                        <div class="dbox w-100 d-flex align-items-center" style="gap: 20px">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" style="color:green"
                                                    fill="currentColor" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                                    <path
                                                        d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z" />
                                                </svg>
                                            </div>
                                            <div class="text pl-3 pt-3">
                                                <p><a target="_blank" href="https://wa.me/+22955505005"
                                                        class="text-dark text-decoration-none">+ 229 55505005</a></p>
                                            </div>
                                        </div>
                                        <div class="dbox w-100 d-flex align-items-center" style="gap: 20px">
                                            <div class="icon d-flex align-items-center justify-content-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                    fill="currentColor" class="bi bi-globe" viewBox="0 0 16 16">
                                                    <path
                                                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z" />
                                                </svg>
                                            </div>
                                            <div class="text pl-3 pt-3 ">
                                                <p><a target="_blank" href="https://moov-africa.bj/"
                                                        class="text-dark text-decoration-none">moov-africa.bj</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 d-flex align-items-stretch">
                                    <div class="contact-wrap w-100 p-md-5 p-4">
                                        <div id="form-message-warning" class="mb-4"></div>
                                        @if (session()->has('success'))
                                            <div id="form-message-success" class="mb-4" style="color: green">
                                                {{ session()->get('success') }}
                                            </div>
                                        @endif

                                        <form action="{{ route('new.email') }}" method="POST" id="contactForm"
                                            name="contactForm">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12 mt-2">
                                                    <div class="form-floating">
                                                        <input name="name" type="text" class="form-control"
                                                            id="floatingInput" placeholder="xxxxxxxx" />
                                                        <label for="floatingInput">Nom et prénom</label>
                                                    </div>
                                                    @error('name')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-12 mt-2">
                                                    <div class="form-floating">
                                                        <input name="email" type="email" class="form-control"
                                                            id="floatingInput" placeholder="xxxxxxxx" />
                                                        <label for="floatingInput">Email</label>
                                                    </div>
                                                    @error('email')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-12 mt-2">
                                                    <div class="form-floating">
                                                        <input name="raison" type="text" class="form-control"
                                                            id="floatingInput" placeholder="xxxxxxxx" />
                                                        <label for="floatingInput">Raison</label>
                                                    </div>
                                                    @error('raison')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-12 mt-2">
                                                    <div class="form-floating col-12">
                                                        <textarea class="form-control" name="message" placeholder="Leave a comment here" id="floatingTextarea2"
                                                            style="height: 100px"></textarea>
                                                        <label for="floatingTextarea2">Message</label>
                                                    </div>
                                                    @error('message')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-12 mt-2">
                                                    <div class="form-group">
                                                        <input style="background-color:#005aa4e7" type="submit"
                                                            value="Envoyer" class="btn text-light">
                                                        <div class="submitting"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
