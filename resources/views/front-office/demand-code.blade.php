@extends('layouts.app')
@section('content')
@if ($success)
<div style="height: 73vh">
    <div class="card col-12 mx-auto col-md-6 col-lg-6" style="margin-top: 110px">
        <div class="card-header" id="demand_code">
            Demande envoyée
        </div>
        <div class="card-body">
            Code de la demande : <span class="bold" id="code" style="font-weight: bold">{{ $code
                }}</span>
        </div>
        <button onclick="copyCode()" id="copyBtn" class="btn btn-primary">Copier le
            code</button>
    </div>
</div>
@endif
@if (!$success)
<div style="height: 73vh">
    <div class="card col-12 mx-auto col-md-6 col-lg-6" style="margin-top: 110px">
        <div class="card-header" id="demand_code">
            Erreur
        </div>
        <div class="card-body">
            Message : <span class="bold" id="code" style="font-weight: bold">{{ $message
                }}</span>
        </div>
    </div>
</div>
@endif
@endsection
@include('front-office.js')