<script type="text/javascript">
    function switched(e) {
        if (e.value === '1') {
            document.getElementById('ownerOption').style.display = 'block';
            document.getElementById('nonOwnerOption').style.display = 'none';
            var signaturePad;
            var signaturePadCanvas = document.querySelector('#signature-pad-canvas');
            var parentWidth = jQuery(signaturePadCanvas).parent().outerWidth();
            signaturePadCanvas.setAttribute("width", parentWidth);
            signaturePad = new SignaturePad(signaturePadCanvas);
        }

        if (e.value === "0") {
            document.getElementById('ownerOption').style.display = 'none';
            document.getElementById('nonOwnerOption').style.display = 'block';
            document.getElementById('validateBtn').style.display = 'block';
            document.getElementById('validateBtn1').style.display = 'none';
        }
    }

    function copyCode() {
        var copyText = document.getElementById("code");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.textContent;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("copy");
        textArea.remove();
        $('#copyBtn').html("Code copié")
    }

    function printErrorMsg(msg) {
        const items = [];
        for (const [key, value] of Object.entries(msg)) {
            $('.' + key + '_err').text(value).show();
            var elmnt = $('.' + key + '_err');
            console.log(elmnt.closest('.form-group'));
            items.push(elmnt.closest('.form-group'))
        }
        if (items[0] !== undefined) {
            items[0].get(0).scrollIntoView({
                behavior: "instant",
                block: "end",
                inline: "nearest"
            })
        }
    }

    var signaturePad;
    var signaturePadCanvas = document.querySelector('#signature-pad-canvas');
    var parentWidth = jQuery(signaturePadCanvas).parent().outerWidth();
    signaturePadCanvas.setAttribute("width", parentWidth);
    signaturePad = new SignaturePad(signaturePadCanvas);

    function saveSignature() {
        if (!signaturePad.isEmpty()) {
            var dataURL = signaturePad.toDataURL();
            $("#signature_field").val(dataURL)
            $("#validateBtn1").show();
            $('#notification').hide()
        }
        if (signaturePad.isEmpty()) {
            $('#notification').show()
        }
    }

    function clean() {
        if (!signaturePad.isEmpty()) {
            signaturePad.clear()
            $("#signature_field").val('');
            $("#validateBtn1").hide();
        }
    }

    function showLoader() {
        $("#validateBtn").hide();
        $("#spinner").show();
    }

    function showLoader1() {
        $("#validateBtn1").hide();
        $("#spinner1").show();
    }

    var myModal = document.getElementById('myModal')
    var myInput = document.getElementById('myInput')

    myModal.addEventListener('shown.bs.modal', function() {
        myInput.focus()
    })
</script>
