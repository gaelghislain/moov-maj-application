@extends('layouts.app')
@section('content')
    <section style="margin-top: 110px; min-height:100vh">
        <div class="container h-60">
            <div class="row d-flex justify-content-center align-items-center h-60">
                <div class="col">
                    <div class="card card-registration my-4">
                        <div class="row g-0">
                            <div class="col-xl-6">
                                <img src="{{ asset('front-office/img/7727d2c969.jfif') }}" alt="Sample photo"
                                    class="img-fluid d-none d-xl-block"
                                    style="
                                        border-top-left-radius: 0.25rem;
                                        border-bottom-left-radius: 0.25rem;
                                    " />
                            </div>
                            <div class="col-xl-6 d-flex align-items-center">
                                <div class="card-body p-md-5 text-black">
                                    <div class="text-center">
                                        <h3 class="mb-5 text-uppercase">Demande de mise à jour</h3>
                                    </div>
                                    <form action="{{ route('demand.save') }}" method="POST" enctype="multipart/form-data"
                                        name="myForm" id="myForm">
                                        @csrf
                                        <div class="mb-4">
                                            <div class="form-floating">
                                                <input value="{{ old('number_to_change') }}" name="number_to_change"
                                                    type="number" class="form-control" id="floatingInput"
                                                    placeholder="xxxxxxxx" />
                                                <label for="floatingInput">Numéro à mettre à jour</label>
                                            </div>
                                        </div>
                                        @error('number_to_change')
                                            <p class="text-danger text-sm">{{ $message }}</p>
                                        @enderror
                                        <div class="justify-content-start align-items-center mb-4 py-2">
                                            <div class="col-12 d-flex justify-content-start">
                                                <h6 class="mb-0">Photo de votre pièce d'identité :</h6>
                                            </div>
                                            <div class="d-flex align-items-center g-2">
                                                <div class="col-2">Recto :</div>
                                                <div class="form-check form-check-inline mb-0 col-10 mt-2 ml-2 p-0">
                                                    <input value="{{ old('user_picture_recto') }}" name="user_picture_recto"
                                                        accept="image/*" class="form-control" type="file"
                                                        id="formFile" />
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-center g-2">
                                                <div class="col-2">Verso :</div>
                                                <div class="form-check form-check-inline mb-0 col-10 mt-2 ml-2 p-0">
                                                    <input value="{{ old('user_picture_verso') }}" name="user_picture_verso"
                                                        accept="image/*" class="form-control" type="file"
                                                        id="formFile" />
                                                </div>
                                            </div>
                                        </div>
                                        @error('user_picture')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        <div class="col align-self-start mb-4 py-2">
                                            <div class="d-flex justify-content-start">
                                                <h6 class="mb-3">
                                                    Connaissez-vous le propriétaire du numéro ? :
                                                </h6>
                                            </div>

                                            <div class="d-flex justify-content-start">
                                                <div class="form-check form-check-inline mb-0 me-4">
                                                    <input class="form-check-input" type="radio" onchange="switched(this)"
                                                        name="is_number_owner" id="yes" value="1" />
                                                    <label class="form-check-label" for="yes">Oui</label>
                                                </div>

                                                <div class="form-check form-check-inline mb-0 me-4">
                                                    <input class="form-check-input" type="radio" onchange="switched(this)"
                                                        name="is_number_owner" id="no" value="0" />
                                                    <label class="form-check-label" for="no">Non</label>
                                                </div>

                                            </div>
                                            @error('is_number_owner')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div id="ownerOption" style="display: none">
                                            <div class="form-floating mb-4">
                                                <input name="owner_number" value="{{ old('owner_number') }}" type="number"
                                                    id="form3Example9" class="form-control form-control-lg"
                                                    placeholder="Nom" />
                                                <label class="form-label" for="form3Example9">Numéro actuel du
                                                    propriétaire</label>
                                            </div>
                                            @error('owner_number')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            <div class="justify-content-start align-items-center mb-4 py-2">
                                                <div class="col-12 d-flex justify-content-start">
                                                    <h6 class="mb-0">Sa pièce d'identité :</h6>
                                                </div>
                                                <div class="d-flex align-items-center g-2">
                                                    <div class="col-2">Recto :</div>
                                                    <div class="form-check form-check-inline mb-0 col-10 mt-2 ml-2 p-0">
                                                        <input value="{{ old('owner_id_picture_recto') }}" accept="image/*"
                                                            name="owner_id_picture_recto" class="form-control"
                                                            type="file" id="formFile" />
                                                    </div>
                                                </div>
                                                @error('owner_id_picture_recto')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                                <div class="d-flex align-items-center g-2">
                                                    <div class="col-2">Verso :</div>
                                                    <div class="form-check form-check-inline mb-0 col-10 mt-2 ml-2 p-0">
                                                        <input value="{{ old('owner_id_picture_verso') }}"
                                                            accept="image/*" name="owner_id_picture_verso"
                                                            class="form-control" type="file" id="formFile" />
                                                    </div>
                                                </div>
                                                @error('owner_id_picture_verso')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <h6 class="mb-3">
                                                Votre signature :
                                            </h6>
                                            <div class="signature-pad-canvas-wrapper">
                                                <canvas id="signature-pad-canvas"></canvas>
                                            </div>
                                            <p class="text-danger" style='display:none' id='notification'>Veuillez signer
                                            </p>
                                            @error('signature')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            <button type="button" class="btn btn-md my-2"
                                                onClick="saveSignature()">Enregistrer la
                                                signature</button>
                                            <button type="button" class="btn btn-md my-2" onClick="clean()">Effacer la
                                                signature</button>

                                            <input id="signature_field" name="signature" type="hidden">

                                        </div>

                                        <div class="col align-self-start mb-4 py-2" id="nonOwnerOption"
                                            style="display: none">
                                            <div class="d-flex justify-content-start">
                                                <h6 class="mb-3">Comment avez-vous obtenu le numéro ?</h6>
                                            </div>

                                            <div class="d-flex justify-content-start">
                                                <select name="obtain_mode" class="form-select form-select-lg"
                                                    id="">
                                                    <option value="Achat avec enregistrement déjà effectué">
                                                        Achat avec enregistrement déjà effectué
                                                    </option>
                                                    <option value="Don">Don</option>
                                                    <option value="Héritage">Héritage</option>
                                                    <option value="Cession après déccès">
                                                        Cession après déccès
                                                    </option>
                                                </select>
                                                @error('obtain_mode')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="d-flex justify-content-end pt-3">
                                                <button onclick="showLoader()" type="submit" id="validateBtn"
                                                    class="btn btn-md ms-2" style="display: none">
                                                    Envoyer
                                                </button>
                                                <button id="spinner" class="btn btn-primary" type="button" disabled
                                                    style="display: none">
                                                    <span class="spinner-border spinner-border-sm" role="status"
                                                        aria-hidden="true"></span>
                                                    Envoi...
                                                </button>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end pt-3">
                                            <button onclick="showLoader1()" type="submit" id="validateBtn1"
                                                class="btn btn-md ms-2" style="display: none">
                                                Envoyer
                                            </button>
                                            <button id="spinner1" class="btn btn-primary" type="button" disabled
                                                style="display: none">
                                                <span class="spinner-border spinner-border-sm" role="status"
                                                    aria-hidden="true"></span>
                                                Envoi...
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('front-office.js')
@endsection
