@extends('layouts.app')
@section('content')
    @if ($success)
        <div style="height: 79vh">
            <div class="text-center">
                <h3 class="text-bold mx-auto mt-5">Résultat de la recherche</h3>
            </div>
            <div class="card col-10 mx-auto mt-5 col-md-6 col-lg-6">
                <div class="card-header" id="demand_code">
                    Code de la demande : <span class="bold" style="font-weight: bold">{{ $data->code }}</span>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Demande de mise à jour du numéro : <span
                            class="badge bg-secondary">{{ $data->number_to_change }}</span></h5>
                    <h5>Statut de la demande : <span class="badge bg-secondary">{{ ucfirst($data->status) }}</span></h5>
                    @if ($data->treatment)
                        <h5>Message: <span class="badge bg-secondary">{{ ucfirst($data->treatment) }}</span></h5>
                    @endif
                    @if ($data->observation)
                        <h5>Observation: <span class="badge bg-secondary">{{ ucfirst($data->observation) }}</span></h5>
                    @endif
                </div>
            </div>
        </div>
    @endif
    @if (!$success)
        <div style="height: 75vh">
            <div class="text-center">
                <h3 class="text-bold mx-auto mt-5">Résultat de la recherche</h3>
            </div>
            <div class="card col-10 mx-auto mt-5 col-md-6 col-lg-6">
                <div class="card-header" id="demand_code">
                    Demande non trouvée</span>
                </div>
            </div>
        </div>
    @endif
@endsection
