 <!-- Sidebar -->
 <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

     <!-- Sidebar - Brand -->
     <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
         <div class="sidebar-brand-icon rotate-n-15">
             <img src="https://esim-front.webcoom.net/assets/img/logo-moov.png" alt="" title=""
                 style="width: 100px" />
         </div>
         <div class="sidebar-brand-text mx-3">Backoffice MAJ <sup>E-SIM</sup></div>
     </a>

@can('demand-treatment')
<!-- Divider -->
<hr class="sidebar-divider">
<!-- Divider -->
<div class="sidebar-heading">
 Agent 
</div>
<!-- Heading -->
<!-- Nav Item - Charts -->
<li class="nav-item">
    <a class="nav-link" href="/treatment">
        <i class="fas fa-fw fa-table"></i>
        <span> Traiter une demande</span></a>
</li>
@endcan

@can('agent-treatment-list')
<li class="nav-item">
    <a class="nav-link" href="/agent-treated-list">
        <i class="fas fa-fw fa-table"></i>
        <span> Mes Demandes Traitées</span></a>
</li>
@endcan

<li class="nav-item">
        <a class="nav-link" href="/customers-message">
        <i class="fas fa-fw fa-table"></i>
        <span>Messages des abonnés</span></a>
</li>

@if (Auth::user()->hasRole('Admin'))
        <hr class="sidebar-divider d-none d-md-block">
        <div class="sidebar-heading">
        Admin 
        </div>
@endif

@can('demands-list')
        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="/awaiting-list">
                <i class="fas fa-fw fa-table"></i>
                <span>En attente de traitement</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/treated-list">
                <i class="fas fa-fw fa-table"></i>
                <span>Traitées</span></a>
        </li>
@endcan

@can('agent-point')
        <hr class="sidebar-divider d-none d-md-block">
        <!-- Heading -->
        <div class="sidebar-heading">
        Points
        </div>

        <li class="nav-item">
            <a class="nav-link" href="/agent-point">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Point par agent</span></a>
        </li>
@endcan

@can('add-user')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Paramètres Utilisateur</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('users.index') }}">Gestion Utilisateur</a>
                    @if (Auth::user()->hasRole('Admin'))
                    <a class="collapse-item" href="{{ route('roles.index') }}">Gestion Rôle</a>
                    @endif
                </div>
            </div>
        </li>
@endcan

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">



     <!-- Sidebar Toggler (Sidebar) -->
     <div class="text-center d-none d-md-inline">
         <button class="rounded-circle border-0" id="sidebarToggle"></button>
     </div>


 </ul>
 <!-- End of Sidebar -->
