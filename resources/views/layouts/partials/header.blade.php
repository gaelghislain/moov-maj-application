<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top p-3 text-dark"
    style="box-shadow: 0px 5px 5px rgb(189 189 189 / 34%);">
    <div class="container-fluid col-lg-10 col-md-12 col-sm-12">
        <div id="logo">
            <a href="/"><img src="{{ asset('front-office/favicon.png') }}" alt=""
                    title="" style="width: 100px" /></a>
        </div>
        <button style="background-color: #fff !important" class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item text-center">
                    <a href="/"
                        class="nav-link m-2 active @if ($name === 'home') is-active @endif">Accueil
                    </a>
                </li>
                <li class="nav-item text-center">
                    <a href="/new-demand"
                        class="nav-link m-2 active @if ($name === 'demand.new') is-active @endif">Nouvelle
                        demande</a>
                </li>
                <li class="nav-item text-center">
                    <a href="/seach-demand"
                        class="nav-link m-2  active @if ($name === 'demand.search') is-active @endif">Rechercher
                        demande</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
