<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('front-office/favicon.png') }}" type="image/x-icon">
    <title>MAJ | Moov Benin</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('front-office/css/jquery.signaturepad.css') }}">
    <link rel="stylesheet" href="{{ asset('front-office/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-office/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-office/css/bunny.css') }}">
    <link rel="stylesheet" href="{{ asset('front-office/css/philosopher.css') }}">
    <script src="{{ asset('front-office/js/jquery.min.js') }}"></script>
    <script src="{{ asset('front-office/js/signature_pad.min.js') }}"></script>
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }

        .tools a {
            text-decoration: none;
        }

        #colors_sketch {
            border: 1px solid #ccc;
        }

        .is-active {
            background-color: #005aa4e7;
            border-radius: 3px;
            color: #fff !important;
            box-shadow: 0 3px 7px rgb(0 0 0 / 10%);
        }

        .is-active {
            background-color: #005aa4e7;
            border-radius: 3px;
            color: #fff !important;
            box-shadow: 0 3px 7px rgb(0 0 0 / 10%);
        }

        h3 {
            color: rgb(0, 91, 164);
        }

        button,
        .badge {
            background-color: #005aa4e7 !important;
            color: #fff !important;
        }

        #signature-pad {
            border: 2px solid;
            width: 100%;
        }

        .signature-pad-canvas-wrapper {
            margin: 15px 0 0;
            border: 1px solid #cbcbcb;
            border-radius: 3px;
            overflow: hidden;
            position: relative;
        }

        .signature-pad-canvas-wrapper::after {
            content: 'Name';
            border-top: 1px solid #cbcbcb;
            color: #cbcbcb;
            width: 100%;
            margin: 0 15px;
            display: inline-flex;
            position: absolute;
            bottom: 10px;
            font-size: 13px;
            z-index: -1;
        }
    </style>
</head>

<body>
    @include('layouts.partials.header')
    @yield('content')
    @include('layouts.partials.footer')
    <script src="{{ asset('front-office/js/bootstrap.min.js') }}"></script>
</body>

</html>
