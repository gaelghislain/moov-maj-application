<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="{{ asset('front-office/favicon.png') }}" type="image/x-icon">
    <title>Backoffice MAJ E-SIM MOOV</title>

    <!-- Custom fonts for this template-->
    <link href=" {{ asset('backoffice/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <script src="{{ asset('backoffice/vendor/jquery/jquery.js') }}"></script>

    <!-- Custom styles for this page -->
    <link href="{{ asset('backoffice/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('backoffice/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.partials.nav-backoffice')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.partials.header-back')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                @yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.partials.footer-backoffice')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
 
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('backoffice/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('backoffice/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('backoffice/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('backoffice/js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('backoffice/vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('backoffice/js/demo/chart-area-demo.js') }}"></script>
    <script src="{{ asset('backoffice/js/demo/chart-pie-demo.js') }}"></script>

    <!-- datatables plugins -->
    <script src="{{ asset('backoffice/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('backoffice/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backoffice/js/demo/datatables-demo.js') }}"></script>
</body>

</html>

