@extends('layouts.app-backoffice')
@section('content')

                <div class="container-fluid">
                    
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="col-mb-2">
                                <a class="btn btn-primary" href="{{ route('roles.index') }}"> Les Rôles </a>
                            </div>
                        </div>
                        
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="card-body">
                        <div class="row">
                            <h3 class="font-weight-bold text-primary">Créer un Rôle </h3>
                        </div>

                        {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Nom:</strong>
                                    {!! Form::text('name', null, array('placeholder' => 'Nom','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Permission:</strong>
                                    <br/>
                                    @foreach($permission as $value)
                                        <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                                        {{ $value->name }}</label>
                                    <br/>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Créer</button>
                            </div>
                        </div>

                        {!! Form::close() !!}

                        </div>
                    </div>

                </div>

@endsection