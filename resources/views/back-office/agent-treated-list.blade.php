@extends('layouts.app-backoffice')
@section('content')
<div class="container-fluid">

                
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Mes Demandes MAJ Traitées</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Numéro</th>
                                            <th>Code</th>
                                            <th>Traitement</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                

<script>

 $(document).ready(function(){

    $('#dataTable').DataTable({
        responsive: true,
        orderCellsTop: true,
        fixedHeader: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "order": [[0, 'desc']],
        "ajax" : {
            url:"{{url('/agent-treatment')}}",
            dataType:'JSON'
        },
      
        "columns":[
            { "data": "treatment_at" },
            { "data": "number_to_change" },
            { "data": "code" },
            { "data": "treatment" },
            { "data": "status" }
        ],
        "columnDefs":[
            {
               render : function(data, type, row, meta){
                 if (data===null){
                   return "Pas de Resultats";
                  } else {
                    return data;
                  }
              }
            },
        ],

    });

 });

</script>

               
@endsection