@extends('layouts.app-backoffice')
@section('content')

    <div class="container-fluid" style="min-height: 80vh">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"></h1>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <h3 class="m-0 font-weight-bold text-primary col-md-8">Traitement MAJ</h3>
                    <h3 class="m-0 font-weight-bold col-md-4" style="color:orange">Total du Jour: {{ $total_treated }}</h3>
                </div>
            </div>
            <div class="card-body">
                @if (!$num_info)
                    <div class="h3 alert alert-info text-center p-4"><i class="fa fa-danger"></i> Pas de numéro disponible !
                    </div>
                @else
                    <form action="javascript:save_treatment();" id="save_treatment">
                        @csrf
                        <div class="card border-info mt-3">
                            <div class="card-header bg-info text-white">
                                Informations sur l'abonné
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="number_to_change" style="font-weight:bold">Nunéro à mettre à jour
                                            :</label>
                                        <input type="text" name="id_number_to_change" class="form-control"
                                            id="id_number_to_change" value="{{ $num_info->id }}" hidden>
                                        <input type="text" name="number_to_change" class="form-control"
                                            id="number_to_change" value="{{ $num_info->number_to_change }}" disabled>
                                    </div>
                                </div>
                                
                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <div class="card mx-auto" style="width: 18rem;">
                                            <label for="" style="font-weight:bold">Pièce d'identité
                                                (Recto)</label>
                                            <img class="card-img-top" style=" float: left;"
                                                src="{{ $num_info->user_picture_recto }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card mx-auto" style="width: 18rem;">
                                            <label for="" style="font-weight:bold">Pièce d'identité
                                                (Verso)</label>
                                            <img class="card-img-top" style=" float: left;"
                                                src="{{ $num_info->user_picture_recto }}">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        @if ($num_info->is_number_owner === 1)
                            <div class="card border-info mt-3" id="know_owner">
                                
                                    <div class="card-header bg-info text-white">
                                        Informations sur le propriétaire
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                        
                                            <div class="col-md-4">
                                                <label for="is_number_owner">L'abonné connait le propriétaire du numéro :</label>
                                                <input type="text" name="is_number_owner" class="form-control"
                                                    id="is_number_owner" value="OUI" disabled>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="owner_number">Numéro actuel du propriétaire :</label>
                                                <input type="text" name="owner_number" class="form-control"
                                                    id="owner_number" value="{{ $num_info->owner_number }}" disabled>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="card mx-auto" style="width: 18rem;">
                                                    <label for="" style="font-weight:bold">Signature</label>
                                                    <img class="card-img-top border" style=" float: left;"
                                                        src="{{ $num_info->signature }}">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row mt-2">

                                                <div class="col-md-6">
                                                    <div class="card mx-auto" style="width: 18rem;">
                                                        <label for="" style="font-weight:bold">Pièce d'identité
                                                            (Recto)</label>
                                                        <img class="card-img-top" style=" float: left;"
                                                            src="{{ $num_info->owner_id_picture_recto }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="card mx-auto" style="width: 18rem;">
                                                        <label for="" style="font-weight:bold">Pièce d'identité
                                                            (Verso)</label>
                                                        <img class="card-img-top" style=" float: left;"
                                                            src="{{ $num_info->owner_id_picture_recto }}">
                                                    </div>
                                                </div>
                                           
                                        </div>

                                    </div>
                              
                            </div>
                        @else
                            <div class="row mt-3" id="not_know_owner">
                                <div class="col-md-6">
                                    <label for="rejet_reason" style="font-weight:bold">Mode d'obtention du numéro :</label>
                                    <input type="text" name="obtain_mode" class="form-control" id="obtain_mode"
                                        value="{{ $num_info->obtain_mode }}" disabled>
                                </div>
                            </div>
                        @endif
                        <div class="card border-primary mt-3" id="know_owner">
                            <div class="card-header bg-primary text-white">
                                Traitement MAJ
                            </div>
                            <div class="card-body mt-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="treatment" style="font-weight:bold">Traitement :</label>
                                        <select name="treatment" class="form-control" id="treatment" required>
                                            <option value="MAJ EFFECTUEE">MAJ EFFECTUEE</option>
                                            <option value="ID NON CONFORME">ID NON CONFORME</option>
                                            <option value="ABONNE ORIENTE EN AGENCE">ABONNE ORIENTE EN AGENCE</option>
                                            <option value="CLIENT A RAPPELER"> CLIENT A RAPPELER</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="Observation" style="font-weight:bold">Observation :</label>
                                        <textarea name="observation" class="form-control" id="Observation"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Enregistrer</button>
                    </form>
                @endif
            </div>
        </div>




        <script type="text/javascript">
            function save_treatment() {

                var donnees = $("form#save_treatment").serialize();

                $.ajax({
                    url: "{{ route('saveTreatment') }}",
                    method: 'POST',
                    data: donnees,
                    beforeSend: function(data) {},
                    success: function(data) {
                        window.location.href = "{{ url('/treatment') }}";
                    },
                    error: function(data) {}
                });
            }
        </script>

    @endsection
