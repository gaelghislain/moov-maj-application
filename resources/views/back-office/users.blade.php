@extends('layouts.app-backoffice')
@section('content')
<div class="container-fluid">
                    <div class="col-md-12 col-xs-12">
                        <h6 class="m-0 font-weight-bold text-primary">Gestion Utilisateur</h6>
                    </div>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="col-mb-2">
                                <a class="btn btn-primary" href="{{ route('users.create') }}"> Ajouter un utilisateur</a>
                            </div>
                        </div>
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>N</th>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Rôles</th>
                                            <th width="280px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $key => $user)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                    <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                            <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Voir</a>
                                            <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Modifier</a>
                                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                                    {!! Form::submit('Supprimer', ['class' => 'btn btn-danger']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                

<script>
       
        var date_debut = document.getElementById('date_debut');
        var date_fin = document.getElementById('date_fin');

        if (date_debut != null) {
            date_debut.max = new Date().toISOString().split("T")[0];
        }
        if (date_fin != null) {
            date_fin.max = new Date().toISOString().split("T")[0];
        }

 </script>
               
@endsection