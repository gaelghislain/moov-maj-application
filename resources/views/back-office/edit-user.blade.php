@extends('layouts.app-backoffice')
@section('content')
<div class="container-fluid">
                   
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="col-mb-2">
                                <a class="btn btn-primary" href="{{ route('users.index') }}"> Les utilisateurs </a>
                            </div>
                        </div>
                        
                        <div class="card-body">

                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                                @endif

                                    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
                                    
                                        @csrf
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-md-6">
                                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}                                            </div>
                                            <div class="col-md-6">
                                            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}                                            </div>
                                        </div>

                                        <div class="form-group mt-3">
                                        <strong>Rôle:</strong>
                                            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
                                        </div>

                                        <button class="btn btn-primary mt-3">
                                            Enregistrer
                                        </button>

                                    {!! Form::close() !!}

                        </div>
                        
                    </div>

                </div>

                

<script>
       
        var date_debut = document.getElementById('date_debut');
        var date_fin = document.getElementById('date_fin');

        if (date_debut != null) {
            date_debut.max = new Date().toISOString().split("T")[0];
        }
        if (date_fin != null) {
            date_fin.max = new Date().toISOString().split("T")[0];
        }

 </script>
               
@endsection