@extends('layouts.app-backoffice')
@section('content')
<div class="container-fluid">
                    <div class="col-md-12 col-xs-12">
                        
                    </div>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="col-mb-2">
                                <a class="btn btn-primary" href="{{ route('users.index') }}"> Les utilisateurs</a>
                            </div>
                        </div>
                        
                        <div class="card-body">

                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                                @endif


                                 <form action="{{ route('users.store') }}" method="POST" class="user">
                                     
                                        @csrf
                                        
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="name" placeholder="Nom Complet" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-md-6">
                                                <input type="password" class="form-control" name="password" placeholder="Mot de passe" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="password" class="form-control" name="confirm-password" placeholder="Confirmer Mot de passe" required>
                                            </div>
                                        </div>

                                        <div class="form-group mt-3">
                                            <strong>Role:</strong>
                                            {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                                        </div>
                                
                                        <button class="btn btn-primary mt-3">
                                            Enregistrer
                                        </button>

                                 </form>
                            
                        </div>
                    </div>

                </div>

                

<script>
       
        var date_debut = document.getElementById('date_debut');
        var date_fin = document.getElementById('date_fin');

        if (date_debut != null) {
            date_debut.max = new Date().toISOString().split("T")[0];
        }
        if (date_fin != null) {
            date_fin.max = new Date().toISOString().split("T")[0];
        }

 </script>
               
@endsection