@extends('layouts.app-backoffice')
@section('content')
<div class="container-fluid">
                   
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="col-mb-2">
                                <a class="btn btn-primary" href="{{ route('users.index') }}"> Les utilisateurs </a>
                            </div>
                        </div>
                        
                        <div class="card-body">
                           <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Nom:</strong>
                                        {{ $user->name }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Email:</strong>
                                        {{ $user->email }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <strong>Rôles:</strong>
                                        @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $v)
                                                <label class="badge badge-success">{{ $v }}</label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                

<script>
       
        var date_debut = document.getElementById('date_debut');
        var date_fin = document.getElementById('date_fin');

        if (date_debut != null) {
            date_debut.max = new Date().toISOString().split("T")[0];
        }
        if (date_fin != null) {
            date_fin.max = new Date().toISOString().split("T")[0];
        }

 </script>
               
@endsection