@extends('layouts.app-backoffice')
@section('content')
    <div class="container-fluid">

        <div class="col-md-12 col-xs-12">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="date_debut" style="font-weight:bold">De :</label>
                    <input id="date_debut" name="date_debut" placeholder="Date_debut" class="form-control" type="date"
                        autocomplete="off" required="" value="<?php echo !empty($_POST['date_debut']) ? trim($_POST['date_debut']) : ''; ?>">
                </div>

                <div class="form-group col-md-4">
                    <label for="date_fin" style="font-weight:bold">à :</label>
                    <input id="date_fin" name="date_fin" placeholder="Date_fin" class="form-control" type="date"
                        autocomplete="off" required="" value="<?php echo !empty($_POST['date_fin']) ? trim($_POST['date_fin']) : ''; ?>">
                </div>

                <div class="form-group col-md-4">
                    <label for="agent" style="font-weight:bold">Agent :</label>
                    <select name="agent" class="form-control" id="agent" required>
                        <option value="TOUT">TOUT</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <button style="font-weight: bold;" type="button" class="btn btn-primary" id="search-button"
                        name="rechercher">Rechercher</button>
                </div>
            </div>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Point par agent</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nom de l'agent</th>
                                <th>Total des demandes traitées</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>



    <script>
        var date_debut = document.getElementById('date_debut');
        var date_fin = document.getElementById('date_fin');
        var agent = document.getElementById('agent');
        if (date_debut != null) {
            date_debut.max = new Date().toISOString().split("T")[0];
        }
        if (date_fin != null) {
            date_fin.max = new Date().toISOString().split("T")[0];
        }

        $(document).ready(function() {
            $("#search-button").click(function() {
                var start_date = $("#date_debut").val();
                var end_date = $("#date_fin").val();
                var agent = $("#agent").val();
                $.ajax({
                    type: 'GET',
                    url: '{{ route('search') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "start_date": start_date,
                        "end_date": end_date,
                        "agent": agent
                    },
                    success: function(data) {
                        $("#dataTable").DataTable()
                        var table = $('#dataTable').DataTable();
                        table.clear();
                        table.rows.add(data).draw();
                    }
                });
            });
        });


        $(document).ready(function() {

            $('#dataTable').DataTable({
                responsive: true,
                orderCellsTop: true,
                fixedHeader: true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
                "order": [
                    [0, 'desc']
                ],
                "ajax": {
                    url: "{{ url('/agent-points') }}",
                    dataType: 'JSON',
                },

                "columns": [{
                        "data": "name"
                    },
                    {
                        "data": "total"
                    },
                ],
                "columnDefs": [{
                    render: function(data, type, row, meta) {
                        if (data === null) {
                            return "Pas de Resultats";
                        } else {
                            return data;
                        }
                    }
                }, ],

            });

        });
    </script>
@endsection
