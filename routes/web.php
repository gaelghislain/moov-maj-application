<?php

use App\Http\Controllers\DemandControler;
use App\Http\Controllers\DemandTreatmentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Mailcontroller;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $name = Route::currentRouteName();
    return view('front-office.welcome', [
        'name' => $name,
        'success' => false,
    ]);
})->name('home');

Route::get('/seach-demand', [DemandControler::class, 'SearchDemandView'])->name('demand.search');
Route::post('/seach-demand', [DemandControler::class, 'searchDemand'])->name('search.submit');
Route::post('/save-demand', [DemandControler::class, 'save'])->name('demand.save');
Route::get('/demand-code', [DemandControler::class, 'demandCodeView'])->name('demand.code');
Route::get('/result', [DemandControler::class, 'demandCodeView'])->name('search.result');
Route::get('/new-demand', [DemandControler::class, 'newDemandView'])->name('demand.new');

Route::post('/new-mail', [MailController::class, 'send'])->name('new.email');


 //backoffice routes
 //Authentification
 Route::get('login', [AuthController::class, 'index'])->name('login');
 Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post'); 
 Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post'); 
 Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    //admin
    Route::get('/index', [AuthController::class, 'index_backoffice'])->name('/index');
    Route::get('/awaiting-list', function () { return view('back-office.awaiting-list');})->name('/awaiting-list');
    Route::get('/awaiting-demand-list', [DemandTreatmentController::class, 'awaiting_demand'])->name('awaiting-demand-list');

    Route::get('/treated-list', function () { return view('back-office.treated-list');})->name('/treated-list');
    Route::get('/treated-demand-list', [DemandTreatmentController::class, 'treated_demand'])->name('treated-demand-list');

    Route::get('/rejected-list', function () { return view('back-office.rejected-list');})->name('/rejected-list');
    Route::get('/rejected-demand-list', [DemandTreatmentController::class, 'rejected_demand'])->name('rejected-demand-list');

    Route::get('/agent-demand-treated', [DemandTreatmentController::class, 'agent_demand_treated'])->name('agent-demand-treated');
    Route::get('/all-demand-treated', [DemandTreatmentController::class, 'all_demand_treated'])->name('all-demand-treated');

    Route::get('/agent-point', function () { return view('back-office.agent-point');})->name('/agent-point');
    Route::get('/agent-points', [DemandTreatmentController::class, 'pointByAgent'])->name('/agent-points');
    Route::get('/search', [DemandTreatmentController::class, 'search'])->name('search');

    //Agent
    Route::get('/treatment', [DemandTreatmentController::class, 'treatment'])->name('/treatment');
    Route::post('saveTreatment', [DemandTreatmentController::class, 'save_treatment'])->name('saveTreatment');
    Route::get('/agent-treated-list', function () { return view('back-office.agent-treated-list');})->name('/agent-treated-list');
    Route::get('/customers-message', function () { return view('back-office.customers-message');})->name('/customers-message');
    Route::get('/agent-treatment', [DemandTreatmentController::class, 'agent_treatment'])->name('agent-treatment');


    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
   
    Route::get('/mail-list', [Mailcontroller::class, 'getMailList'])->name('/mail-list');
});
