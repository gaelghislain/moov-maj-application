<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demands', function (Blueprint $table) {
            $table->id();
            $table->integer('number_to_change');
            $table->string('user_picture_recto');
            $table->string('user_picture_verso');
            $table->boolean('is_number_owner');
            $table->integer('owner_number')->nullable();
            $table->string('owner_id_picture_recto')->nullable();
            $table->string('owner_id_picture_verso')->nullable();
            $table->string('obtain_mode')->nullable();
            $table->string('status')->default('enregistré');
            $table->string('code');
            $table->text('signature')->nullable();
            $table->boolean('select_num')->default(0);
            $table->boolean('treat')->default(0);
            $table->string('treatment_at')->nullable();
            $table->string('treatment')->nullable();
            $table->string('observation')->nullable();
            $table->string('agentId')->nullable();
            $table->string('agentName')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demands');
    }
};
