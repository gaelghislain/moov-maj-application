<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Mail\CustomerMail;
use Illuminate\Http\Request;
use App\Http\Requests\MailRequest;
use App\Models\Mail as ModelsMail;
use Illuminate\Support\Facades\Mail;

class Mailcontroller extends Controller
{
    public function send(MailRequest $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'raison' => $request->raison,
            'message' => $request->message,
        ];
        $mail = new ModelsMail();
        $mail->name = $request->name;
        $mail->email = $request->email;
        $mail->raison = $request->raison;
        $mail->message = $request->message;
        $mail->save();   
        return redirect()->back()->with('success', 'Message envoyé avec succès.');
    }

    public function getMailList() {
        $mailList = ModelsMail::all();
        $reponse = json_encode(array('data' => $mailList), true);
        return $reponse;
    }
}
