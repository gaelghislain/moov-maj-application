<?php

namespace App\Http\Controllers;
use Session;
use stdClass;
use App\Models\User;
use App\Models\Demand;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


class DemandTreatmentController extends Controller
{

    function __construct()
    {
       
        $this->middleware('permission:agent-treatment-list|demand-treatment', ['only' => ['agent_treatment','treatment']]);
   
    }

    public function awaiting_demand()
    {
        $awaiting_demand = Demand::where('status','enregistré')->get();
        $reponse = json_encode(array('data' => $awaiting_demand), true);
        return $reponse;
    }

    public function treated_demand()
    {
        $date = date("Y-m-d");
        $date_debut = $date . ' 00:00:00';
        $date_fin = $date . ' 23:59:59';

        $awaiting_demand = Demand::whereIn('status',['traité'])
        ->whereBetween('created_at', [$date_debut, $date_fin])
        ->get();
        $reponse = json_encode(array('data' => $awaiting_demand), true);
        return $reponse;
    }
    
    public function agent_treatment()
    {
        $date = date("Y-m-d");
        $date_debut = $date . ' 00:00:00';
        $date_fin = $date . ' 23:59:59';

        $treated_demand = Demand::where('status','traité')
        ->whereBetween('treatment_at', [$date_debut, $date_fin])
        ->where('agentId', Auth::user()->id)
        ->get();

        $reponse = json_encode(array('data' => $treated_demand), true);
        return $reponse;
    }

    public function rejected_demand()
    {
        $date = date("Y-m-d");
        $date_debut = $date . ' 00:00:00';
        $date_fin = $date . ' 23:59:59';

        $awaiting_demand = Demand::where('status','rejected')
        ->whereBetween('created_at', [$date_debut, $date_fin])
        ->get();
        dd($awaiting_demand);
        $reponse = json_encode(array('data' => $awaiting_demand), true);
        return $reponse;
    }

    public function treatment(){

        $select_num = Demand::where('status','enregistré')
        ->where(function ($query) {
              $query->where('select_num',0)
                    ->where('treat',0)
                    ->whereNull('treatment_at')
                    ->whereNull('agentId');
        })
        ->orWhere(function ($query) {
              $query->where('select_num',1)
                    ->where('treat',0)
                    ->whereNull('treatment_at')
                    ->where('agentId',Auth::user()->id);
        })
        ->orderBy('id')
        ->first();
        
       // dd( $select_num);

        if ($select_num ) {
            $select_num->update([
                'select_num' => 1 ,
                'agentId' => Auth::user()->id
            ]);
        }

        $date = date("Y-m-d");
        $date_debut = $date . ' 00:00:00';
        $date_fin = $date . ' 23:59:59';
        
        $total_treated = Demand::where('agentId',Auth::user()->id)
        ->where('status','traité')
        ->whereBetween('treatment_at', [$date_debut, $date_fin])
        ->count();

        return view('back-office.treatment')->with(['num_info' => $select_num, 'total_treated' => $total_treated]);
        
    }

    public function save_treatment(Request $request){

        $info_num = Demand::where('id',$request->id_number_to_change)->first();
        $maj_num = $info_num->update([
            'treat' => 1 ,
            'status' => 'traité' ,
            'treatment_at' => date("Y-m-d H:i:s"),
            'treatment' => $request->treatment,
            'observation' => $request->observation,
            'agentId' => Auth::user()->id,
            'agentName' => Auth::user()->name
        ]);

    }

    public function pointByAgent()
    {
        $pointByAgent = Demand::where('status','traité')->get();
        $pointByAgent = $pointByAgent->groupBy('agentName');
        $data = collect();
        foreach($pointByAgent as $key => $value) {
            $object = new stdClass();
            $object->name = $key;
            $object->total = $value->count();
            $data->push($object);
        }
        return json_encode(array('data' => $data), true);
    }

    public function search(Request $request)
    {
    $start_date = $request->start_date. ' 00:00:00';
    $end_date = $request->end_date. ' 23:59:59';
    $agent = $request->agent;

    // Effectuer la requête de recherche avec les paramètres fournis
    $pointByAgent = Demand::where('status', 'traité')
            ->wherebetween('treatment_at', [$start_date, $end_date])
            ->get();
    $pointByAgent = $pointByAgent->groupBy('agentName');
    $data = collect();
        foreach($pointByAgent as $key => $value) {
            $object = new stdClass();
            $object->name = $key;
            $object->total = $value->count();
            $data->push($object);
        }
    return response()->json($data);
    }
}
