<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
   
    /**
     * @return response()
     */
    public function index()
    {
        return view('back-office.login');
    }  
      
    /**
     * @return response()
     */
    public function registration()
    {
        return view('back-office.registration');
    }
      
    /**
     * @return response()
     */
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
   
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            
            if (Auth::user()->hasRole('Admin')) {
                return redirect()->intended('index')
                             ->withSuccess('Vous avez réussi à vous connecter');
            }
            else {
                return redirect()->intended('treatment')
                             ->withSuccess('Vous avez réussi à vous connecter');
            }
            
        }
  
        return redirect("login")->withErrors('Vous avez saisi des informations d\'identification invalides');
    }
      
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function postRegistration(Request $request)
    {  
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);
           
        $data = $request->all();
        $check = $this->create($data);
         
        return redirect("treatment")->withSuccess('Vous avez réussi à vous connecter');
    }
    
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index_backoffice()
    {
        if(Auth::user()->hasRole('Admin')){
            return view('back-office.index');
        }
  
        return redirect("login")->withSuccess('Vous n\'avez pas accès');
    }
    
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function create(array $data)
    {
      return User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }
    
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function logout() {
        Session::flush();
        Auth::logout();
        return Redirect('login');
    }
}
