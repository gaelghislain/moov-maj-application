<?php

namespace App\Http\Controllers;
// use Barryvdh\DomPDF\Facade as PDF;
use PDF;
use App\Models\Demand;
// use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\DemandRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class DemandControler extends Controller
{
    public function SearchDemandView()
    {
        $name = Route::currentRouteName();
        return view('front-office.search-demand', [
            'name' => $name,
        ]);
    }

    public function searchresultView()
    {
        return view('front-office.search-result');
    }

    public function demandCodeView()
    {
        return view('front-office.demand-code');
    }

    public function newDemandView()
    {
        $name = Route::currentRouteName();
        return view('front-office.new-demand', [
            'name' => $name,
        ]);
    }

    public function save(DemandRequest $request)
    {
        try {
            $user_picture_path_recto = $request->file('user_picture_recto')->store('image', 'public');
            $user_picture_path_verso = $request->file('user_picture_verso')->store('image', 'public');
            $demand = new Demand();
            $demand->number_to_change = $request->number_to_change;
            $demand->user_picture_recto = asset(Storage::url( $user_picture_path_recto));
            $demand->user_picture_verso = asset(Storage::url($user_picture_path_verso));
            $demand->is_number_owner = $request->is_number_owner;
            $demand->code = Str::random(5) . date('Ymd');
            
            if ($demand->is_number_owner == '1') {
                $owner_id_picture_path_recto = $request->file('owner_id_picture_recto')->store('image', 'public');
                $owner_id_picture_path_verso = $request->file('owner_id_picture_verso')->store('image', 'public');
                $demand->owner_number = $request->owner_number;
                $demand->owner_id_picture_recto = asset(Storage::url($owner_id_picture_path_recto));
                $demand->owner_id_picture_verso = asset(Storage::url($owner_id_picture_path_verso));
                $demand->signature = $request->signature;
            }
            if ($demand->is_number_owner == '0') {
                $demand->obtain_mode = $request->obtain_mode;
            }
            $demand->save();
            return view('front-office.demand-code', [
                'name' => 'home',
                'success' => true,
                'code' => $demand->code
            ]);
        } catch (\Throwable $e) {
            return view('front-office.demand-code', [
                'name' => 'home',
                'success' => false,
                'message' => $e->getMessage()
            ]);

        }
    }

    public function searchDemand(Request $request)
    {
        $request->validate([
            'demand_code' => 'required|string',
        ]);

        try {
            $result = Demand::where('code', $request->demand_code)->orwhere('number_to_change', $request->demand_code)->first();
            if ($result) {
                return view('front-office.search-result', [
                    'success' => true,
                    'data' => $result,
                    'name' => "demand.search"
                ]);
            } else {
                return view('front-office.search-result', [
                    'success' => false,
                    'name' => "demand.search"
                ]);
            }
        } catch (\Throwable $e) {
            return $e->getMessage();
        }
    }

    // public function generatePDF($image1, $image2)
    // {
    //     // Load the image files
    //     $images = [
    //         "public/front-office/img/moovmaj.png", "public/front-office/img/moovmaj.png"
    //     ];

    //     // Create the pdf file
    //     $pdf = PDF::loadView('front-office.pdf', compact('images'));
    //     $pdf->save(storage_path('app/public/image/images.pdf'));
    //     // Get the pdf content
    //     $pdf_content = $pdf->output();
    //     // Save the pdf in the database
    //     // $pdf_model = new PdfModel();
    //     // $pdf_model->name = 'images.pdf';
    //     // $pdf_model->pdf = $pdf_content;
    //     // $pdf_model->save();

    //     // Return the pdf file
    //     dd('succes');
    //     return $pdf->download('images.pdf');
    // }

}
