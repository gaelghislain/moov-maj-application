<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:30',
            'email' => 'required|email',
            'raison' => 'required|string|max:500',
            'message' => 'required|string|max:2000',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Veuillez renseigner votre nom',
            'email.required' => 'Veuillez renseigner votre adresse mail',
            'raison.required' => 'Veuillez renseigner la raison de votre message',
            'message.required' => 'Veuillez renseigner votre message',
            'name.string' => 'Le nom doit être une chaîne de caractères',
            'raison.string' => 'La raison doit être une chaîne de caractères',
            'message.string' => 'Le message doit être une chaîne de caractères',
            'email.email' => 'Adresse email invalide',
            'name.max' => 'Le nombre de caractère maximale est 30',
            'raison.max' => 'Le nombre de caractère maximale est 100',
            'message.max' => 'Le nombre de caractère maximale est 2000',
        ];
    }
}
