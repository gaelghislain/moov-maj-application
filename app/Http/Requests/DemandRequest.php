<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DemandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'number_to_change' => 'required|digits:8|unique:demands',
            'user_picture_recto' => 'required|image|mimes:jpeg,jpg,png,gif|max:2048',
            'user_picture_verso' => 'required|image|mimes:jpeg,jpg,png,gif|max:2048',
            'is_number_owner' => 'required|string|min:1',
            'owner_number' => 'exclude_if:is_number_owner,0|required|digits:8',
            'owner_id_picture_recto' => 'exclude_if:is_number_owner,0|required|image|mimes:jpeg,jpg,png,gif|max:2048',
            'owner_id_picture_verso' => 'exclude_if:is_number_owner,0|required|image|mimes:jpeg,jpg,png,gif|max:2048',
            'obtain_mode' => 'exclude_if:is_number_owner,1|string|required',
            'signature' => 'exclude_if:is_number_owner,0|required',
        ];
    }

    public function messages()
    {
        return [
            'number_to_change.required' => 'Veuillez renseigner le numéro à mettre à jour',
            'number_to_change.unique' => 'Ce numéro a été déjà mis à jour',
            'number_to_change.digits' => 'Le numéro doit contenir 8 caractères',
            'user_picture.required' => "La photo d'identité est obligatoire",
            'user_picture.image' => "Veuiller télecharger une image",
            'user_picture.max' => "Échec du téléchargement de l'image. La taille d'image maximale est de 2 Mo.",
            'owner_number.required' => "Le numéro du propriétaire est obligatoire",
            'owner_number.digits' => "Le numéro du propriétaire doit contenir 8 caractères",
            'owner_id_picture.required' => "La photo de la pièce d'identité est obligatoire",
            'owner_id_picture.image' => "Veuiller télecharger une image",
            'owner_id_picture.max' => "Échec du téléchargement de l'image. La taille d'image maximale est de 2 Mo.",
            'obtain_mode.required' => "Veuillez renseigner le moyen d'obtention du numéro",
            'signature.required' => "La signature est obligatoire"
        ];
    }
}
