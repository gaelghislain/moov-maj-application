<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodeOtp extends Model
{
    use HasFactory;

    protected $fillable = [
        'number_to_change',
        'code',
    ];
}
