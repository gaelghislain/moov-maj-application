<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    use HasFactory;

    protected $fillable = [
        'number_to_change',
        'user_picture',
        'is_number_owner',
        'owner_number',
        'owner_id_picture',
        'obtain_mode',
        'status',
        'code',
        'signature',
        'select_num',
        'treat',
        'treatment_at',
        'treatment',
        'observation',
        'agentId',
        'agentName'
    ];  
}
